import tornado.ioloop
import tornado.web
import subprocess
import threading
import time
from multiprocessing import Event
from Queue import Queue, Empty as EmptyError
import shlex
from binascii import hexlify
from ConfigParser import ConfigParser
from tornado.escape import json_decode, json_encode
import requests
import argparse
import os

try:
    import json
except ImportError:
    import simplejson as json


NO_BODY = 1
TYPE_CLI = 'cli'
TYPE_REQUEST = 'request'

class Global:
    """ Global variables/scope.
        - processing: List of processes running and HTTP 
          request being sent or receiving.
        - queue: List of processing waiting to be executed.
          They are dequeued once they enter in "processing".
        - tabs: List of possible processes to be executed.
          They come from configuration file.
    TODO: Improve scope, so we can separate classes
          in different file.
    """
    processing = []
    queue = Queue()
    tabs = {}


class Tarea(object):
    """ Based on a system process pipe, it calls a CLI command
    and receive the "stderr", "stdout" and can send "stdin".
    Also it counts the time process have been running before it
    exists. There is a unique "token" that identify this
    specific instance. """
    __slots__ = ['id', 'pipe', 'time_start', 'time_end',
                 'token']

class RequestThread(threading.Thread):
    """ A process can be a background request. Which uses
    module 'requests' to send and receive HTTP requests. """
    __slots__ = ['id', 'time_start', 'time_end', 'request',
                 'finished', 'response']

    def __init__(self, request):
        super(RequestThread, self).__init__()
        self.finished = False
        self.request = request

    def run(self):
        s = requests.Session()
        try:
            request = s.prepare_request(self.request)
            response = s.send(request)
            self.response = response
            print(response.status_code)
        except requests.ConnectionError:
            print("error")

        self.finished = True


class MainThread(threading.Thread):
    """ Main Loop of daemon it spawns all of the 
    processes and background requests. Itself is a thread."""

    def __init__(self, *args, **kwargs):
        super(MainThread, self).__init__()
        self.event = Event()
        config = ConfigParser()
        confile = kwargs['config_file'] \
                  if 'config_file' in kwargs \
                  else '../tests/default.ini'
        config.readfp(open(confile))
        for section in config.sections():
            if section.startswith('process:'):
                section_name = section[8:]
                tab_type = config.get(section, 'type')

                if tab_type == TYPE_CLI:
                    tab = {'command': config.get(section, 'command'),
                           'type': TYPE_CLI}
                    if config.has_option(section, 'user'):
                        tab['user'] = config.get(section, 'user')

                elif tab_type == TYPE_REQUEST:
                    tab = {'uri': config.get(section, 'uri'),
                           'type': TYPE_REQUEST}
                
                Global.tabs[section_name] = tab

        self.config = config

    def run(self):
        while not self.event.is_set():
            self.iteration()

    def iteration(self):
        """ This is the everytime execute statement, it checks
        if there is new requests in Global.queue, if so, then
        checks if it's a CLI or Request and instance the
        respective classes and add them to process. Finally
        call the sync process. """

        try:
            item, token, options = Global.queue.get(timeout=0)
            tab = Global.tabs[item]
            tab_type = tab['type']

            if tab_type == TYPE_CLI:
                if 'user' in tab:
                    command = ['su', tab['user'], '-c', tab['command']]
                else:
                    command = shlex.split(Global.tabs[item]['command'])

                tarea = Tarea()
                tarea.pipe = subprocess.Popen(command,
                                stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE)
                
                tarea.time_start = time.time()
                tarea.time_end = time.time()
                tarea.token = token

                Global.processing.append(tarea)

            elif tab_type == TYPE_REQUEST:
                request = requests.Request('GET', tab['uri'])
                tarea = RequestThread(request)
                tarea.start()
                
            Global.processing.append(tarea)

        except EmptyError:
            time.sleep(1)

        self.handle_processes_sync()

    def handle_processes_sync(self):
        """ Pool open pipes and active requests waiting them
        to finish, where pipe must be returncode different to
        None, and Request a fail or a server response """
        
        toremove = []
        for tarea in Global.processing:
            if isinstance(tarea, RequestThread):
                if tarea.finished:
                    toremove.append(tarea)

            elif isinstance(tarea, Tarea):
                returncode = tarea.pipe.poll()
                if returncode is not None:
                    toremove.append(tarea)
                    print("done (token: {})".format(tarea.token))
                else:
                    tarea.time_end = time.time()

        for tarea in toremove:
            Global.processing.remove(tarea)

    def stop(self):
        self.event.set()

class EnqueueHandler(tornado.web.RequestHandler):
    def post(self):
        try:
            data = json_decode(self.request.body)
            process_name = data.get('processName', None)
            options = data.get('options', None)
            token = hexlify(os.urandom(5))
            if process_name in Global.tabs:
                toenqueue = (process_name, token, options)
                Global.queue.put(toenqueue)
            body = {'success': True, 'token': token}
            
        except ValueError:
            body = {'success': False, 'reason': NO_BODY}

        self.add_header('Content-Type', 'application/json')
        self.write(json_encode(body))

        
urls = [
    (r'^/enqueue$', EnqueueHandler),
]

if __name__ == '__main__':
    aparser = argparse.ArgumentParser()
    aparser.add_argument('-c', type=str)
    parsed = aparser.parse_args()

    app = tornado.web.Application(urls)
    loop = tornado.ioloop.IOLoop()
    try:
        mthread = MainThread(config_file=parsed.c)
        mthread.start()
        app.listen(4545)
        loop.start()
    except KeyboardInterrupt:
        mthread.stop()
        print("bye")